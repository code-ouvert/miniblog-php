<?php
    session_start();
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "You are already logged in";
        header('Location: admin/index.php');
    }
    $pageTitle = "Login";
    include('includes/header.php');
?>

<div class="container">
    <div class="row mb-4">
        <div class="col-md-6 mx-auto">
            <h3 class="text-center mt-5 mb-4 h1">Please login</h3>
            <p class="text-center lead mb-4">Your administration area is waiting for you</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <?php
                if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
                    $message = $_SESSION['flash_message'];
                    $type = $_SESSION['flash_type'];
                    unset($_SESSION['flash_message']);
                    unset($_SESSION['flash_type']);
            ?>
            <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
                <div>
                    <?php echo $message; } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mb-4">
        <div class="col-md-6 mx-auto">
            <form action="treatment/treatment_login.php" method="POST">
                <div class="mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" id="username">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Email</label>
                    <input type="password" class="form-control" name="password" id="password">
                </div>
                <button type="submit" class="btn btn-warning">Login</button>
            </form>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>