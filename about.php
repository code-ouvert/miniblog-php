<?php
    session_start();
    //include('database/connexion.php');
    $pageTitle = "A Propos";
    include('includes/header.php');
?>

<!--main part-->
<div class="container">
    <h3 class="text-center mb-5 h1">About me ?</h3>

    <h4 class="mb-4 h2">Who am I ?</h4>

    <p class="lead mb-4">Lorem ipsum, dolor sit amet consectetur.</p>

    <p class="lead mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur porro perspiciatis ratione vero voluptatibus nulla.</p>

    <p class="lead mb-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Corporis, esse id quisquam non ex excepturi consequuntur veritatis dolore.</p>

    <p class="lead mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel obcaecati doloribus in, assumenda, modi aperiam impedit veniam vero repudiandae, iste officia suscipit facilis corporis quam?</p>

    <h4 class="mb-4 h2">My adventures!</h4>

    <p class="lead mb-4">Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Debitis est labore ut nihil, ullam et perferendis? Tempore, eaque laudantium. Aperiam explicabo culpa ullam repudiandae numquam!</p>

    <p class="lead mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>

    <p class="lead mb-4">Lorem ipsum dolor sit amet consectetur adipisicing, elit. Laudantium nostrum temporibus, minima corporis adipisci a mollitia quaerat perferendis architecto. Eum?</p>

    <h4 class="mb-4 h2">Open Sesame </h4>

    <p class="lead mb-4">Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Dolor, repudiandae libero impedit adipisci dolorem debitis.</p>

    <p class="lead mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro ipsa iusto facilis ea illo. Adipisci magnam iure saepe fugiat ullam repudiandae quibusdam ab commodi cum numquam.</p>

    <p class="lead mb-4 fw-bold">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo, necessitatibus minima debitis inventore.</p>
</div>

<?php include('includes/footer.php'); ?>