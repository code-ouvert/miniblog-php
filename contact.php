<?php
    session_start();
    $pageTitle = "Contact";
    include('includes/header.php');
?>

<div class="container">
    <h3 class="text-center mb-4 h1">Contact me</h3>
    <h4 class="mb-4 h3">My contact details</h4>
    <ul>
        <li class="lead">Phone number : <a class="text-decoration-none text-warning" href="#">+237 681 07 60 39</a></li>
        <li class="lead">E-mail address: <a a class="text-decoration-none text-warning" href="#">leyassino@gmail.com</a></li>
    </ul>

    <h4 class="mb-4 h3">You need...</h4>

    <h5 class="mb-4 h4">A developer for a web application project?</h5>

    <p class="lead mb-4">Are you looking for a Freelance web developer to materialize an idea, an application project? I can accompany you on a project from A to Z, from design to production. I work with the Symfony framework (PHP). <a class="text-warning" href="#">Submit a development request</a></p>

    <h5 class="mb-4 h4">Tailor-made training?</h5>

    <p class="lead mb-4">Are you looking for training for your development teams? I can produce tailor-made educational content for you (Texts, Tutorials and videos) on many themes: Docker, CI/CD, Development environment, Symfony, Project management... <a class="text-warning" href="#">Make a training request</a></p>

    <h5 class="mb-4 h4">Partnership for the YouTube channel?</h5>

    <p class="lead mb-4">Are you looking for visibility on YouTube (and Twitter) with an audience of French-speaking developers? Do not hesitate to contact me ! <a class="text-warning" href="#">Propose a partnership</a></p>
</div>

<div id="form"></div>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="ow mt-5 mb-4">
        <div class="col-md-8 mx-auto">
            <div class="alert alert-<?php echo $type ?>" role="alert">
                <div>
                    <?php echo $message; } ?>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="container">
    <div class="row mt-5 mb-4">
        <div class="col-md-8 mx-auto">
            <form action="treatment/treatment_contact.php" method="POST">
                <div class="mb-3">
                    <label for="name" class="form-label">Full name</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" name="email" id="email">
                </div>
                <div class="mb-3">
                    <label for="subject" class="form-label">Subject</label>
                    <input type="text" class="form-control" name="subject" id="subject">
                </div>

                <div class="mb-3">
                    <label for="message" class="form-label">Message</label>
                    <textarea class="form-control" name="message" id="message" style="height: 150px"></textarea>
                </div>

                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>