<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Messages";
    include('includes/header.php');

    $sql = "SELECT * FROM contacts ORDER BY created_at DESC";
    $contacts = $db->query($sql);
    if($contacts === false){
        die("Erreur");
    }
?>

<div class="container">
    <h3 class="text-center mb-4 h1">Messages</h3>

    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">Messages</th>
                <th scope="col">Created_at</th>
            </tr>
        </thead>
        <tbody>
            <?php while($row = $contacts->fetch(PDO::FETCH_ASSOC)) : ?>
            <tr>
                <td><?php echo htmlspecialchars($row['name']); ?></td>
                <td><?php echo htmlspecialchars($row['email']); ?></td>
                <td><?php echo htmlspecialchars($row['subject']); ?></td>
                <td><?php echo htmlspecialchars($row['message']); ?></td>
                <td><?php echo htmlspecialchars($row['created_at']); ?></td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    
</div>
