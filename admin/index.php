<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }

    include('../database/connexion.php');
    $pageTitle = "Articles";
    include('includes/header.php');

    $sql = "SELECT * FROM articles ORDER BY created_at DESC";
    $articles = $db->query($sql);
    if($articles === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Articles</h3>

    <a class="btn btn-warning mb-4" href="new_articles.php">Create new article</a>

    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">Titles</th>
                <th scope="col">Authors</th>
                <th scope="col">Categories</th>
                <th scope="col">Tags</th>
                <th scope="col">Created_at</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php while($row = $articles->fetch(PDO::FETCH_ASSOC)) : ?>
            <tr>
                <td>
                    <?php 
                        echo htmlspecialchars($row['title']); 
                    ?>
                </td>
                <td>
                    <?php
                        $query = $db->prepare('SELECT * FROM users WHERE id = :id');
                        $query->execute([
                            'id' => $row['user_id'],
                        ]);
                        $query = $query->fetch(PDO::FETCH_ASSOC);

                        echo htmlspecialchars($query['username']);
                    ?>
                </td>
                <td>
                    <?php
                        $query = $db->prepare('SELECT * FROM categories WHERE id = :id');
                        $query->execute([
                            'id' => $row['category_id'],
                        ]);
                        $query = $query->fetch(PDO::FETCH_ASSOC);

                        echo htmlspecialchars($query['name']);
                    ?>
                </td>
                <td>
                    <?php
                        $article_id = htmlspecialchars($row['id']);
                        
                        $sql = "SELECT COUNT(*) as article_id FROM article_tags WHERE article_id = $article_id";
                        $count = $db->query($sql);
                        $count = $count->fetchColumn();

                        echo $count;
                    ?>
                    <a class="text-decoration-none text-info" href="add_tags.php?id=<?php echo htmlspecialchars($row['id']); ?>">Add</a>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['created_at']); ?>
                </td>
                <td>
                    <a class="text-decoration-none text-warning" href="update_articles.php?id=<?php echo htmlspecialchars($row['id']); ?>">Edit</a>
                    <a class="text-decoration-none text-danger" href="delete_articles.php?id=<?php echo htmlspecialchars($row['id']); ?>">Delete</a>
                </td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    
</div>
