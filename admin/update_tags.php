<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Update tags";
    include('includes/header.php');

    $getData = $_GET['id'];

    if (!isset($getData) && is_numeric($getData))
    {
        echo('It requires a tags id to modify it.');
        return;
    }

    // $sql = "SELECT * FROM tags WHERE id = $getData";
    // $tags = $db->query($sql);
    // if($tags === false){
    //     die("Erreur");
    // }

    $query = $db->prepare('SELECT * FROM tags WHERE id = :id');
    $query->execute([
        'id' => $getData,
    ]);
    $query = $query->fetch(PDO::FETCH_ASSOC);
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Update tags</h3>

    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">
            <form method="post" action="treatment/treatment_update_tags.php">

                <input type="hidden" class="form-control" name="id" value="<?php echo htmlspecialchars($query['id']); ?>" id="id">
                
                <div class="mb-3">
                    <label for="name" class="form-label">Name of tags</label>
                    <input type="text" class="form-control" value="<?php echo htmlspecialchars($query['name']); ?>" name="name" id="name">
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
