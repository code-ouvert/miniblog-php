<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Update category";
    include('includes/header.php');

    $getData = $_GET['id'];

    if (!isset($getData) && is_numeric($getData))
    {
        echo('It requires a category id to modify it.');
        return;
    }

    $query = $db->prepare('SELECT * FROM categories WHERE id = :id');
    $query->execute([
        'id' => $getData,
    ]);
    $query = $query->fetch(PDO::FETCH_ASSOC);
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Update category</h3>
    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">
            <form method="post" action="treatment/treatment_update_categories.php">

                <input type="hidden" class="form-control" name="id" value="<?php echo htmlspecialchars($query['id']); ?>" id="id">

                <div class="mb-3">
                    <label for="name" class="form-label">Name of category</label>
                    <input type="text" class="form-control" name="name" value="<?php echo htmlspecialchars($query['name']); ?>" id="name">
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
