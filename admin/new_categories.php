<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    $pageTitle = "New category";
    include('includes/header.php');
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Create new category</h3>

    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">

            <form method="post" action="treatment/treatment_new_categories.php">
                <div class="mb-3">
                    <label for="name" class="form-label">Name of category</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>

                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
