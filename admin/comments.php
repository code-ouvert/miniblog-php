<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Comments";
    include('includes/header.php');

    $sql = "SELECT * FROM comments ORDER BY created_at DESC";
    $comments = $db->query($sql);
    if($comments === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Comments</h3>
    
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">Articles</th>
                <th scope="col">Names</th>
                <th scope="col">Emails</th>
                <th scope="col">Contents</th>
                <th scope="col">Created_at</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php while($row = $comments->fetch(PDO::FETCH_ASSOC)) : ?>
            <tr>
                <td>
                    <?php
                        $query = $db->prepare('SELECT * FROM articles WHERE id = :id');
                        $query->execute([
                            'id' => $row['article_id'],
                        ]);
                        $query = $query->fetch(PDO::FETCH_ASSOC);

                        echo htmlspecialchars($query['title']);
                    ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['name']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['email']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['content']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['created_at']); ?>
                </td>
                <td>
                    <a class="text-decoration-none text-danger" href="delete_comments.php?id=<?php echo htmlspecialchars($row['id']); ?>">Delete</a>
                </td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    
</div>