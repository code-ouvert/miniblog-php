<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Add tags";
    include('includes/header.php');

    $getData = $_GET['id'];

    if (!isset($getData) && is_numeric($getData))
    {
        echo('It requires a article id to modify it.');
        return;
    }

    $sql = "SELECT * FROM tags ORDER BY id ASC";
    $tags = $db->query($sql);
    if($tags === false){
        die("Erreur");
    }

    $article = $db->prepare('SELECT * FROM articles WHERE id = :id');
    $article->execute([
        'id' => $getData,
    ]);
    $article = $article->fetch(PDO::FETCH_ASSOC);
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>
    
<div class="container">
    <h3 class="text-center mb-4 h1">Add tags to <?php echo htmlspecialchars($article['title']); ?></h3>

    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">

            <form action="treatment/treatment_add_tags.php" method="POST" enctype="multipart/form-data">

                <input type="hidden" class="form-control" name="article_id" value="<?php echo htmlspecialchars($article['id']); ?>" id="user_id">

                <div class="mb-3">
                    <label for="tags" class="form-label">Tags</label>
                    <select class="form-select" name="tags[]" multiple aria-label="multiple select example" size="10" id="tags">
                        <?php while($row = $tags->fetch(PDO::FETCH_ASSOC)) : ?>
                            <?php
                                $article_id = htmlspecialchars($article['id']);
                                $tags_id = htmlspecialchars($row['id']);
                                $sql = "SELECT COUNT(*) as article_id FROM article_tags WHERE article_id = $article_id AND tags_id = $tags_id";
                                $count = $db->query($sql);
                                $count = $count->fetchColumn();
                            ?>
                            <option value="<?php echo htmlspecialchars($row['id']); ?>" <?php if ($count == 1) { echo 'selected'; } ?>><?php echo htmlspecialchars($row['name']); ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
