<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Delete category";
    include('includes/header.php');

    $getData = $_GET['id'];
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Delete category</h3>
    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">
            <form method="post" action="treatment/treatment_delete_categories.php">
                <div class="mb-3 visually-hidden">
                    <label for="id" class="form-label">Id</label>
                    <input type="hidden" class="form-control" name="id" value="<?php echo htmlspecialchars($getData); ?>" id="id">
                </div>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
