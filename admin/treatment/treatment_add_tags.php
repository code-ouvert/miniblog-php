<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['article_id'])){
        $recupArticleId = $_POST['article_id'];
    }
    if(isset($_POST['tags'])){
        $recupTags = $_POST['tags'];
    }

    if(($recupArticleId != "") && ($recupTags != "")){
        foreach ($recupTags as $tags){
            $sql = "SELECT COUNT(*) as article_id FROM article_tags WHERE article_id = $recupArticleId AND tags_id = $tags";
            $count = $db->query($sql);
            $count = $count->fetchColumn();

            if($count == 0){
                $query = $db->prepare("INSERT INTO article_tags (article_id, tags_id) VALUES (:recupArticleId, :tags)");

                $query->bindParam(':recupArticleId', $recupArticleId);
                $query->bindParam(':tags', $tags);

                $query->execute();
            }
        }
        header('Location: ../index.php');
        $_SESSION['flash_type'] = "success";
        $_SESSION['flash_message'] = "Tags successfully Add";
        exit();
    } else {
        header('Location: ../add_tags.php?id=' . $recupArticleId);
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : Can you verify your submission? There are a few issues with this.";
        exit();
    }
?>