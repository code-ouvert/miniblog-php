<?php
    session_start();
    include('./../database/connexion.php');

    // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
    if (isset($_FILES['image']) && $_FILES['image']['error'] == 0 && isset($_POST['title']) && isset($_POST['content']))
    {
        $recupTitle = $_POST['title'];
        $recupContent = $_POST['content'];
        $recupCategory = $_POST['category'];

        // Testons si le fichier n'est pas trop gros
        if ($_FILES['image']['size'] <= 3000000 && ($recupTitle != "" && strlen($recupTitle) <= 100) && ($recupContent != "" && strlen($recupContent) >= 10))
        {
            // Testons si l'extension est autorisée
            $fileInfo = pathinfo($_FILES['image']['name']);
            $extension = $fileInfo['extension'];
            $allowedExtensions = ['jpg', 'jpeg', 'png'];
            if (in_array($extension, $allowedExtensions))
            {
                // On peut valider le fichier et le stocker définitivement
                move_uploaded_file($_FILES['image']['tmp_name'], 'assets/uploads/' . basename($_FILES['image']['name']));
                echo "L'envoi a bien été effectué !";
            }
        }
    }
?>
