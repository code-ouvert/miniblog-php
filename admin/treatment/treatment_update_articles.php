<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['id']) && isset($_POST['title']) && isset($_POST['category']) && isset($_POST['content'])){
        $recupId = $_POST['id'];
        $recupTitle = $_POST['title'];
        $recupCategory = $_POST['category'];
        $recupContent = $_POST['content'];
    }

    if(($recupId != "") && ($recupTitle != "" && strlen($recupTitle) <=100) && ($recupCategory != "") && ($recupContent != "" && strlen($recupContent) >10)){

        $sql = "SELECT COUNT(*) as titles FROM articles WHERE title = '$recupTitle'";
        $count = $db->query($sql);
        $count = $count->fetchColumn();

        if($count <= 1){

            $query = $db->prepare('UPDATE articles SET title = :recupTitle, category_id = :recupCategory, content = :recupContent WHERE id = :recupId');

            $query->execute([
                'recupId' => $recupId,
                'recupTitle' => $recupTitle,
                'recupCategory' => $recupCategory,
                'recupContent' => $recupContent,
            ]);

            if ($query->execute()) {
                header('Location: ../index.php');
                $_SESSION['flash_type'] = "success";
                $_SESSION['flash_message'] = "Articles successfully UPDATED";
                exit();
            } else {
                echo "Unable to create record";
            }
        } else {
            header('Location: ../index.php');
            $_SESSION['flash_type'] = "info";
            $_SESSION['flash_message'] = "Unable to create record. It already exists";
            exit();
        }

    } else {
        header('Location: ../update_articles.php?id=' . $recupId);
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : Can you verify your submission? There are a few issues with this.";
        exit();
    }
?>