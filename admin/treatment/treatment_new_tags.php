<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['name'])){
        $recupName = $_POST['name'];
    }

    if($recupName != "" && strlen($recupName) <=50){

        $sql = "SELECT COUNT(*) as name FROM tags WHERE name = '$recupName'";
        $count = $db->query($sql);
        $count = $count->fetchColumn();

        if ($count == 0) {
            $sqlQuery = $db->prepare("INSERT INTO tags (name) VALUES (:recupName)");

            $sqlQuery->bindParam(':recupName', $recupName);

            if ($sqlQuery->execute()) {
                header('Location: ../tags.php');
                $_SESSION['flash_type'] = "success";
                $_SESSION['flash_message'] = "Category successfully CREATED";
                exit();
                exit();
            } else {
                echo "Unable to create record";
            }
        } else {
            header('Location: ../tags.php');
            $_SESSION['flash_type'] = "info";
            $_SESSION['flash_message'] = "Unable to create record. It already exists";
            exit();
        }
    } elseif (strlen($recupName) > 50) {
        header('Location: ../new_tags.php');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "The field is longer than 50 characters";
        exit();
    } else {
        header('Location: ../new_tags.php');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "The field is empty";
        exit();
    }
?>