<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['id'])){
        $recupId = $_POST['id'];
    }

    $query = $db->prepare('DELETE FROM articles WHERE id = :recupId');

    $query->bindParam(':recupId', $recupId);

    if ($query->execute()) {
        header('Location: ../index.php');
        $_SESSION['flash_type'] = "success";
        $_SESSION['flash_message'] = "Articles successfully DELETE";
    } else {
        echo "Unable to create record";
    }
?>
