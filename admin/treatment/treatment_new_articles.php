<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['user_id']) && isset($_POST['title']) && isset($_POST['category']) && isset($_POST['content']) && isset($_POST['tags'])){
        $recupUserId = $_POST['user_id'];
        $recupTitle = $_POST['title'];
        $recupCategory = $_POST['category'];
        $recupContent = $_POST['content'];
        $recupTags = $_POST['tags'];
    }

    if(($recupUserId != "") && ($recupCategory != "") && ($recupTitle != "" && strlen($recupTitle) <=100) && ($recupContent != "" && strlen($recupTitle) >10)){

        $sql = "SELECT COUNT(*) as title FROM articles WHERE title = '$recupTitle'";
        $count = $db->query($sql);
        $count = $count->fetchColumn();

        if($count == 0){

            foreach ($recupTags as $a){
                echo $a;
            }

            $query = $db->prepare("INSERT INTO articles (user_id, category_id, title, content) VALUES (:recupUserId, :recupCategory, :recupTitle, :recupContent)");

            $query->bindParam(':recupUserId', $recupUserId);
            $query->bindParam(':recupCategory', $recupCategory);
            $query->bindParam(':recupTitle', $recupTitle);
            $query->bindParam(':recupContent', $recupContent);

            if ($query->execute()) {
                header('Location: ../index.php');
                $_SESSION['flash_type'] = "success";
                $_SESSION['flash_message'] = "Articles successfully CREATED";
                exit();
            } else {
                echo "Unable to create record";
            }
        } else {
            header('Location: ../index.php');
            $_SESSION['flash_type'] = "info";
            $_SESSION['flash_message'] = "Unable to create record. It already exists";
            exit();
        }
    } else {
        header('Location: ../new_articles.php');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : Article no create";
        exit();
    }
?>