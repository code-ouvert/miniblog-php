<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['name'])){
        $recupName = $_POST['name'];
    }

    if($recupName != "" && strlen($recupName) <=50){

        $sql = "SELECT COUNT(*) as name FROM categories WHERE name = '$recupName'";
        $count = $db->query($sql);
        $count = $count->fetchColumn();

        if($count == 0){

            $query = $db->prepare("INSERT INTO categories (name) VALUES (:recupName)");

            $query->bindParam(':recupName', $recupName);

            if ($query->execute()) {
                header('Location: ../categories.php');
                $_SESSION['flash_type'] = "success";
                $_SESSION['flash_message'] = "Category successfully CREATED";
                exit();
            } else {
                echo "Unable to create record";
            }
        } else {
            header('Location: ../categories.php');
            $_SESSION['flash_type'] = "info";
            $_SESSION['flash_message'] = "Unable to create record. It already exists";
            exit();
        }
        
    } elseif (strlen($recupName) > 50) {
        header('Location: ../new_categories.php');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "The field is longer than 50 characters";
        exit();
    } else {
        header('Location: ../new_categories.php');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "The field is empty";
        exit();
    }
?>