<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['id'])){
        $recupId = $_POST['id'];
    }

    $query = $db->prepare('DELETE FROM tags WHERE id = :recupId');

    $query->bindParam(':recupId', $recupId);

    if ($query->execute()) {
        header('Location: ../tags.php');
        $_SESSION['flash_type'] = "success";
        $_SESSION['flash_message'] = "Tags successfully DELETE";
    } else {
        echo "Unable to create record";
    }
?>
