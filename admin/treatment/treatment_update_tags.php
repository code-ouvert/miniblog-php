<?php
    session_start();
    include('../../database/connexion.php');

    if(isset($_POST['id']) && isset($_POST['name'])){
        $recupId = $_POST['id'];
        $recupName = $_POST['name'];
    }

    if($recupName != "" && strlen($recupName) <=50){

        $sql = "SELECT COUNT(*) as name FROM tags WHERE name = '$recupName'";
        $count = $db->query($sql);
        $count = $count->fetchColumn();

        if($count == 0){

            $query = $db->prepare('UPDATE tags SET name = :recupName WHERE id = :recupId');

            $query->execute([
                'recupId' => $recupId,
                'recupName' => $recupName,
            ]);

            // query->bindParam(':recupId', $recupId);
            // query->bindParam(':recupName', $recupName);

            if ($query->execute()) {
                header('Location: ../tags.php');
                $_SESSION['flash_type'] = "success";
                $_SESSION['flash_message'] = "Category successfully UPDATED";
                exit();
            } else {
                echo "Unable to create record";
            }
        } else {
            header('Location: ../tags.php');
            $_SESSION['flash_type'] = "info";
            $_SESSION['flash_message'] = "Unable to create record. It already exists";
            exit();
        }
        
    } elseif (strlen($recupName) > 50) {
        header('Location: ../update_tags.php?id=' . $recupId);
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : The field is longer than 50 characters";
        exit();
    } else {
        header('Location: ../update_tags.php?id=' . $recupId);
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : The field is empty";
        exit();
    }
?>