<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Update article";
    include('includes/header.php');

    $userLoginId = $_SESSION['userLoginId'];

    $getData = $_GET['id'];

    if (!isset($getData) && is_numeric($getData))
    {
        echo('It requires a article id to modify it.');
        return;
    }

    $article = $db->prepare('SELECT * FROM articles WHERE id = :id');
    $article->execute([
        'id' => $getData,
    ]);
    $article = $article->fetch(PDO::FETCH_ASSOC);

    $sql = "SELECT * FROM categories ORDER BY id ASC";
    $categories = $db->query($sql);
    if($categories === false){
        die("Erreur");
    }

    $sql = "SELECT * FROM tags ORDER BY id ASC";
    $tags = $db->query($sql);
    if($tags === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>
    
<div class="container">
    <h3 class="text-center mb-4 h1">Update article</h3>

    <div class="row mt-5 mb-4">
        <div class="col-md-10 mx-auto">

            <form action="treatment/treatment_update_articles.php" method="POST" enctype="multipart/form-data">

                <input type="hidden" class="form-control" name="id" value="<?php echo htmlspecialchars($article['id']); ?>" id="id">

                <div class="mb-3">
                    <label for="title" class="form-label">Titre of article</label>
                    <input type="text" class="form-control" value="<?php echo htmlspecialchars($article['title']); ?>" name="title" id="title">
                </div>

                <div class="row">
                    <div class="mb-3 col-md-6">
                        <label for="category" class="form-label">Category</label>
                        <select class="form-select" name="category" aria-label="Default select example">
                            <option selected>
                                <?php echo htmlspecialchars($article['category_id']); ?>
                            </option>
                            <?php while($row = $categories->fetch(PDO::FETCH_ASSOC)) : ?>
                                <option value="<?php echo htmlspecialchars($row['id']); ?>">
                                    <?php echo htmlspecialchars($row['name']); ?>
                                </option>
                            <?php endwhile; ?>
                        </select>
                    </div>
                    <!--
                    <div class="mb-3 col-md-6">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" class="form-control" name="image" id="image"/>
                    </div>
                    -->
                </div>

                <div class="mb-3">
                    <label for="content" class="form-label">Content of article</label>
                    <textarea class="form-control" name="content" id="content" style="height: 300px"><?php echo htmlspecialchars($article['content']); ?></textarea>
                </div>

                <div class="mb-3">
                    <label for="tags" class="form-label">Tags</label>
                    <select class="form-select" name="tags[]" multiple aria-label="multiple select example" id="tags">
                        <?php while($row = $tags->fetch(PDO::FETCH_ASSOC)) : ?>
                            <option value="<?php echo htmlspecialchars($row['id']); ?>"><?php echo htmlspecialchars($row['name']); ?></option>
                        <?php endwhile; ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
        </div>
    </div>
</div>
