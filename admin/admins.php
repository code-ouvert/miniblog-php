<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }

    include('../database/connexion.php');
    $pageTitle = "Admins";
    include('includes/header.php');

    $sql = "SELECT * FROM users ORDER BY created_at DESC";
    $users = $db->query($sql);
    if($users === false){
        die("Erreur");
    }
?>

<div class="container">
    <h3 class="text-center mb-4 h1">Admins</h3>

    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Roles</th>
                <th scope="col">Nbrs of articles</th>
                <th scope="col">Created_at</th>
            </tr>
        </thead>
        <tbody>
            <?php while($row = $users->fetch(PDO::FETCH_ASSOC)) : ?>
            <tr>
                <td><?php echo htmlspecialchars($row['id']); ?></td>
                <td><?php echo htmlspecialchars($row['username']); ?></td>
                <td><?php echo htmlspecialchars($row['roles']); ?></td>
                <td>
                    <?php
                        $id = htmlspecialchars($row['id']);
                        $sql = "SELECT COUNT(*) FROM articles WHERE user_id = $id";
                        $count = $db->query($sql);
                        $count = $count->fetchColumn();
                        echo ($count);
                    ?>
                </td>
                <td><?php echo htmlspecialchars($row['created_at']); ?></td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    
</div>
