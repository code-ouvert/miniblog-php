<?php
    session_start();
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {
        header('Location: ../login.php');
    }
    include('../database/connexion.php');
    $pageTitle = "Categories";
    include('includes/header.php');

    $sql = "SELECT * FROM categories ORDER BY name ASC";
    $categories = $db->query($sql);
    if($categories === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<div class="container">
    <h3 class="text-center mb-4 h1">Categories</h3>

    <a class="btn btn-warning mb-4" href="new_categories.php">Create new category</a>

    <table class="table table-dark table-striped table datatable">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Names</th>
                <th scope="col">Nbrs of articles</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php while($row = $categories->fetch(PDO::FETCH_ASSOC)) : ?>
            <tr>
                <td><?php echo htmlspecialchars($row['id']); ?></td>
                <td><?php echo htmlspecialchars($row['name']); ?></td>
                <td>
                    <?php
                        $id = htmlspecialchars($row['id']);
                        $sql = "SELECT COUNT(*) FROM articles WHERE category_id = $id";
                        $count = $db->query($sql);
                        $count = $count->fetchColumn();
                        echo ($count);
                    ?>  
                </td>
                <td>
                    <a class="text-decoration-none text-warning" href="update_categories.php?id=<?php echo htmlspecialchars($row['id']); ?>">Edit</a>
                    <a class="text-decoration-none text-danger" href="delete_categories.php?id=<?php echo htmlspecialchars($row['id']); ?>">Delete</a>
                </td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    
</div>

