<?php
    session_start();
    include('database/connexion.php');
    $pageTitle = "Articles";
    include('includes/header.php');

    $getData = $_GET['id'];

    if (!isset($getData) && is_numeric($getData))
    {
        echo('It requires a article id to modify it.');
        return;
    }

    $article = $db->prepare('SELECT * FROM articles WHERE id = :id');
    $article->execute([
        'id' => $getData,
    ]);
    $article = $article->fetch(PDO::FETCH_ASSOC);

    $sql = "SELECT * FROM comments WHERE article_id = $getData ORDER BY created_at DESC";
    $comments = $db->query($sql);
    if($comments === false){
        die("Erreur");
    }

    $sql = "SELECT COUNT(*) FROM comments WHERE article_id = $getData";
    $count = $db->query($sql);
    $count = $count->fetchColumn();

    //$sql = "SELECT * FROM articles ORDER BY created_at DESC";
    //$articles = $db->query($sql);
    //if($articles === false){
    //    die("Erreur");
    //}

    $sql = "SELECT * FROM categories ORDER BY name ASC";
    $categories = $db->query($sql);
    if($categories === false){
        die("Erreur");
    }

    $sql = "SELECT * FROM tags ORDER BY name ASC";
    $tags = $db->query($sql);
    if($tags === false){
        die("Erreur");
    }

    $sql = "SELECT * FROM article_tags WHERE article_id = $getData";
    $articles_tags = $db->query($sql);
    if($comments === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<!--main part-->
<div class="container">
    <div class="row">
        <!--Left column-->
        <div class="col-md-8">
        	<h2 class="text-center mb-4 h1"><?php echo htmlspecialchars($article['title']); ?></h2>
	        <p class="text-center mb-4">
	        	<small class="align-top">Posted on <?php echo htmlspecialchars($article['created_at']); ?>
	        		, by 
	        		<span class="fw-bold">
	        			<?php
	                        $user = $db->prepare('SELECT * FROM users WHERE id = :id');
	                        $user->execute([
	                            'id' => $article['user_id'],
	                        ]);
	                        $user = $user->fetch(PDO::FETCH_ASSOC);

	                        echo htmlspecialchars($user['username']);
	                    ?>
	        		</span>
	        		. Date of last modification <?php echo htmlspecialchars($article['updated_at']); ?>
	        	</small>
	        </p>
<!--
	         <div class="ratio ratio-16x9 mb-4">
	             <iframe src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" title="YouTube video" allowfullscreen></iframe>
	         </div>
-->
			<?php echo htmlspecialchars($article['content']); ?>

			<div class="mt-3">
				<span class="me-1 fw-bold fs-5">TAGS :</span>
				<?php while($row = $articles_tags->fetch(PDO::FETCH_ASSOC)) : ?>
					<?php
						$id_tags = htmlspecialchars($row['tags_id']);
	                    $article_tags = $db->prepare('SELECT * FROM tags WHERE id = :id');
	                    $article_tags->execute([
	                        'id' => $id_tags,
	                    ]);
	                    $article_tags = $article_tags->fetch(PDO::FETCH_ASSOC);
	                ?>
					<a href="#"><span class="me-1 badge bg-dark"><?php echo htmlspecialchars($article_tags['name']); ?></span></a>
				<?php endwhile; ?>
			</div>

	        <br/>
	        <br/>

	        <p class="fs-2">
		        <?php
		         	echo $count;
		         	if ($count == 1) {
		          		echo ' COMMENT';
		         	} else {
		          		echo ' COMMENTS';
		         	}       		
		        ?>
	        </p>
	        <hr/>
	        <div class="mt-3">
	        	<?php while($row = $comments->fetch(PDO::FETCH_ASSOC)) : ?>
	        		<div class="d-flex text-muted pt-3">
	      				<svg class="bd-placeholder-img flex-shrink-0 me-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"/><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>

	      				<p class="pb-3 mb-0 small lh-sm border-bottom">
	        				<strong class="d-block text-gray-dark">
	        					<?php echo htmlspecialchars($row['name']); ?>
	        				</strong>
	        				<?php echo htmlspecialchars($row['created_at']); ?>
	        				<br/><?php echo htmlspecialchars($row['content']); ?>
	      				</p>
	    			</div>
	            <?php endwhile; ?>
	        </div>

	        <div class="mt-5">
	        	<h3>Leave a comment</h3>
	         	<form action="treatment/treatment_comment.php" method="POST">
	         		<div class="mb-3 visually-hidden">
				   		<label for="title" class="form-label">Title</label>
				    	<input type="hidden" class="form-control" value="<?php echo($article['title']); ?>" name="title" id="title">
					</div>
	         		<div class="mb-3 visually-hidden">
				   		<label for="article_id" class="form-label">Article id</label>
				    	<input type="hidden" class="form-control" value="<?php echo($article['id']); ?>" name="article_id" id="article_id">
					</div>
					<div class="mb-3">
				   		<label for="name" class="form-label">Full name</label>
				    	<input type="text" class="form-control" name="name" id="name">
					</div>
				    <div class="mb-3">
				    	<label for="email" class="form-label">Email</label>
				    	<input type="text" class="form-control" name="email" id="email">
					</div>
					<div class="mb-3">
				 		<label for="content" class="form-label">Content</label>
						<textarea class="form-control" name="content" id="content" style="height: 150px"></textarea>
					</div>
					<button type="submit" class="btn btn-warning">Submit</button>
				</form>
	      	</div>   
    	</div>

        <!--Right column-->
        <div class="col col-md-4">
            <div class="position-sticky" style="top: 1rem;">
                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">About the Author</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <img class="rounded-circle" src="assets/img/1.png" alt="Code Ouvert" width="140" height="140">
                        </div>
                        <p class="lead" style="text-align: justify">
                            I am <span class="fw-bold">Yassin El Kamal NGUESSU</span> . I live in Yaounde in Cameroon. I am a freelance web developer and trainer, specialized in Symfony. PHP developer, Symfony specialist, I have been hacking web pages professionally since 2020, and for fun for much longer.
                        </p>
                    </div>
                </div>
                        
                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">Categories</h4>
                    </div>
                    <div class="card-body">
                        <?php while($row = $categories->fetch(PDO::FETCH_ASSOC)) : ?>
                            <a class="lead text-decoration-none d-flex justify-content-between align-items-center mb-0 text-warning" href="categories.php?id=<?php echo htmlspecialchars($row['id']); ?>">
                                <?php echo htmlspecialchars($row['name']); ?>
                            </a>
                        <?php endwhile; ?>
                    </div>
                </div>

                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">Tags</h4>
                    </div>
                    <div class="card-body">
                        <?php while($row = $tags->fetch(PDO::FETCH_ASSOC)) : ?>
                            <a href="#"><span class="badge bg-dark"><?php echo htmlspecialchars($row['name']); ?></span></a>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Main part-->

<?php include('includes/footer.php'); ?>