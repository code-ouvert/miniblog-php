<?php
    session_start();
    include('database/connexion.php');
    $pageTitle = "Articles";
    include('includes/header.php');

    $sql = "SELECT * FROM articles ORDER BY created_at DESC";
    $articles = $db->query($sql);
    if($articles === false){
        die("Erreur");
    }

    $sql = "SELECT * FROM categories ORDER BY name ASC";
    $categories = $db->query($sql);
    if($categories === false){
        die("Erreur");
    }

    $sql = "SELECT * FROM tags ORDER BY name ASC";
    $tags = $db->query($sql);
    if($tags === false){
        die("Erreur");
    }
?>

<?php
    if(isset($_SESSION['flash_message']) && isset($_SESSION['flash_type'])) {
        $message = $_SESSION['flash_message'];
        $type = $_SESSION['flash_type'];
        unset($_SESSION['flash_message']);
        unset($_SESSION['flash_type']);
?>
<div class="container">
    <div class="alert alert-<?php echo $type ?> d-flex align-items-center" role="alert">
        <div>
            <?php echo $message; } ?>
        </div>
    </div>
</div>

<!--main part-->
<div class="container">
    <div class="row">
        <!--Left column-->
        <div class="col-md-8">

            <div class="row row-cols-1 row-cols-md-2">
                <?php while($row = $articles->fetch(PDO::FETCH_ASSOC)) : ?>
                    <div class="col">
                        <div class="card mb-4 border-0" style="background-color: #292a2d;">
                            <img src="assets/img/3.png" alt="...">
                            <div class="card-body">
                                <h5 class="card-title h3 fw-bold"><?php echo htmlspecialchars($row['title']); ?></h5>
                                <p class="align-top small">
                                    <span class="fw-bold">
                                        <?php
                                            $query = $db->prepare('SELECT * FROM users WHERE id = :id');
                                            $query->execute([
                                                'id' => $row['user_id'],
                                            ]);
                                            $query = $query->fetch(PDO::FETCH_ASSOC);

                                            echo htmlspecialchars($query['username']);
                                        ?>
                                    </span> 
                                    - 
                                    <span>
                                        <?php echo htmlspecialchars($row['created_at']); ?>  
                                    </span>
                                </p>
                                <a href="show.php?title=<?php echo htmlspecialchars($row['title']); ?>&amp;id=<?php echo htmlspecialchars($row['id']); ?>" class="btn btn-warning stretched-link">Read more</a>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>   
        </div>

        <!--Right column-->
        <div class="col col-md-4">
            <div class="position-sticky" style="top: 1rem;">
                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">About the Author</h4>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <img class="rounded-circle" src="assets/img/1.png" alt="Code Ouvert" width="140" height="140">
                        </div>
                        <p class="lead" style="text-align: justify">
                            I am <span class="fw-bold">Yassin El Kamal NGUESSU</span> . I live in Yaounde in Cameroon. I am a freelance web developer and trainer, specialized in Symfony. PHP developer, Symfony specialist, I have been hacking web pages professionally since 2020, and for fun for much longer.
                        </p>
                    </div>
                </div>
                        
                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">Categories</h4>
                    </div>
                    <div class="card-body">
                        <?php while($row = $categories->fetch(PDO::FETCH_ASSOC)) : ?>
                            <a class="lead text-decoration-none d-flex justify-content-between align-items-center mb-0 text-warning" href="categories.php?id=<?php echo htmlspecialchars($row['id']); ?>">
                                <?php echo htmlspecialchars($row['name']); ?>
                            </a>
                        <?php endwhile; ?>
                    </div>
                </div>

                <div class="card mb-1 rounded-3 shadow-sm border-0" style="background-color: #292a2d;">
                    <div class="card-header py-1 text-white" style="background-color: #252627;">
                        <h4 class="my-0 fw-normal">Tags</h4>
                    </div>
                    <div class="card-body">
                        <?php while($row = $tags->fetch(PDO::FETCH_ASSOC)) : ?>
                            <a class="lead" href="#"><span class="badge bg-secondary"><?php echo htmlspecialchars($row['name']); ?></span></a>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Main part-->

<?php include('includes/footer.php'); ?>