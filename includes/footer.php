        <br/>
        <br/>
        <br/>

        <!--Footer-->
        <div class="container">
            <footer class="py-1 my-3">
                <p class="text-center text-muted">&copy; <?php echo (date('Y')); ?> <a class="text-decoration-none text-warning" href="index.php">MyBlog</a> &#8226; By Yassin El Kamal NGUESSU</p>
            </footer>
        </div>
        <!--End Footer-->

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>

</html>