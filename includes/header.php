<!doctype html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Yassin El Kamal NGUESSU">

        <title><?php echo $pageTitle;?> - MyBlog</title>

        <?php
            //Vérifier si le protocole HTTP ou HTTPS est activé par le serveur avec la variable globale
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
            {
                $url = "https";
            }
            else
            {
                $url = "http";
            }
            // Ajoutez // à l'URL.
            $url .= "://";
            // Ajoutez l'hôte (nom de domaine, ip) à l'URL.
            $url .= $_SERVER['HTTP_HOST'];
            // Ajouter l'emplacement de la ressource demandée à l'URL
            $url .= $_SERVER['REQUEST_URI'];
        ?>
        <link rel="canonical" href="<?php echo $url;?>">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>

    <body style="background-color: #292a2d; color:#a9a9b3">
        <!--Navigation bar-->
        <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #252627;">
            <div class="container">
                <a class="navbar-brand" href="index.php">MyBlog</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link <?php if ($pageTitle == "Articles") { echo "active";} ?>" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($pageTitle == "A Propos") { echo "active";} ?>" href="about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($pageTitle == "Contact") { echo "active";} ?>" href="contact.php">Contact</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav mb-2 mb-lg-0 d-flex">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="admin/index.php">Admin</a>
                        </li>
                        <?php
                            if(isset($_GET['logout']))
                            { 
                                if($_GET['logout']==true)
                                {  
                                    session_unset();
                                    header("location: index.php");
                                }
                            }
                        ?>
                    </ul>
<!--
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-warning" type="submit">Search</button>
                    </form>
-->
                </div>
            </div>
        </nav>
        <!--End Navigation bar-->

        <br/>
        <br/>