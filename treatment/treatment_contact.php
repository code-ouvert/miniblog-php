<?php
    session_start();
    include('../database/connexion.php');

    if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])){
        $recupName = $_POST['name'];
        $recupEmail = $_POST['email'];
        $recupSubject = $_POST['subject'];
        $recupMessage = $_POST['message'];
    }

    if(($recupName != "" && strlen($recupName) <=50) && ($recupEmail != "" && filter_var($recupEmail, FILTER_VALIDATE_EMAIL)) && ($recupSubject != "" && strlen($recupSubject) <=100) && ($recupMessage != "" && strlen($recupMessage) > 10)){

        $query = $db->prepare("INSERT INTO contacts (name, email, subject, message) VALUES (:recupName, :recupEmail, :recupSubject, :recupMessage)");

        $query->bindParam(':recupName', $recupName);
        $query->bindParam(':recupEmail', $recupEmail);
        $query->bindParam(':recupSubject', $recupSubject);
        $query->bindParam(':recupMessage', $recupMessage);

        if ($query->execute()) {
            header('Location: ../index.php#form');
            $_SESSION['flash_type'] = "success";
            $_SESSION['flash_message'] = "Message successfully SEND";
            exit();
        } else {
            echo "Unable to create record";
        }
    } else {
        header('Location: ../contact.php#form');
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : Can you verify your submission? There are a few issues with this.";
        exit();
    }
?>