<?php
    session_start();
    include('../database/connexion.php');

    if(isset($_POST['article_id']) && isset($_POST['title']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['content'])){
        $recupArticleId = $_POST['article_id'];
        $recupTitle = $_POST['title'];
        $recupName = $_POST['name'];
        $recupEmail = $_POST['email'];
        $recupContent = $_POST['content'];
    }

    if(($recupArticleId != "") && ($recupName != "" && strlen($recupName) <=50) && ($recupEmail != "" && filter_var($recupEmail, FILTER_VALIDATE_EMAIL)) && ($recupContent != "" && strlen($recupContent) > 10)){

        $query = $db->prepare("INSERT INTO comments (article_id, name, email, content) VALUES (:recupArticleId, :recupName, :recupEmail, :recupContent)");

        $query->bindParam(':recupArticleId', $recupArticleId);
        $query->bindParam(':recupName', $recupName);
        $query->bindParam(':recupEmail', $recupEmail);
        $query->bindParam(':recupContent', $recupContent);

        if ($query->execute()) {
            header('Location: ../show.php?title=' . $recupTitle . '&id=' .$recupArticleId);
            $_SESSION['flash_type'] = "success";
            $_SESSION['flash_message'] = "Comment successfully CREATED";
            exit();
        } else {
            echo "Unable to create record";
        }
    } else {
        header('Location: ../show.php?title=' . $recupTitle . '&id=' .$recupArticleId);
        $_SESSION['flash_type'] = "danger";
        $_SESSION['flash_message'] = "Error : Comment no post";
        exit();
    }
?>