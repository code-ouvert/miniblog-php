<?php
	$host = 'localhost';
	$dbname = "my_blog";
	$username = 'yassin';
	$password = 'root';
	$sql = "mysql:host=$host;dbname=$dbname";
	$dsn_Options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

	try
	{ 
  		$db = new PDO($sql, $username, $password, $dsn_Options);
	}
	catch (PDOException $e) {
  		die('Erreur : ' . $e->getMessage());
	}
?>