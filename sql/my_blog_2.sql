-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : sam. 20 août 2022 à 01:43
-- Version du serveur : 8.0.29-0ubuntu0.20.04.3
-- Version de PHP : 8.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `my_blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE `articles` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `category_id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `category_id`, `title`, `image_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 6, 'Déterminer si une variable est de type nombre entier', NULL, 'Jusque-là, vous n\'avez travaillé que sur une seule table à la fois. Dans la pratique, vous aurez certainement plusieurs tables dans votre base, dont la plupart seront interconnectées.\r\n\r\nCela vous permettra :\r\n\r\nde mieux découper vos informations ;\r\n\r\nd\'éviter des répétitions ;\r\n\r\net de rendre ainsi vos données plus faciles à gérer.\r\n\r\nPar exemple, dans notre table recipes  , on répète à chaque fois l\'e-mail du contributeur de la recette alors qu\'il est déjà dans la table users .\r\n\r\nEt si on ajoute une gestion des commentaires sur la page d\'une recette, alors il faudra lier le commentaire à un utilisateur et à une recette.\r\n\r\nPour bien comprendre l\'intérêt et la notion de jointure, c\'est la fonctionnalité que vous allez mettre en place dans ce chapitre : suivez le guide !\r\n\r\nModélisez une relation\r\nSi on considère une page qui affiche la recette avec la possibilité que les utilisateurs puissent commenter, voire évaluer la recette, alors un commentaire a les propriétés suivantes :\r\n\r\nun identifiant unique ;\r\n\r\nune recette ;\r\n\r\nun auteur ;\r\n\r\nune date de publication ;\r\n\r\nune note (disons de 0 à 5).\r\n\r\nSi on se représente la table \"comments\", elle ressemblerait à ceci :', '2022-08-14 17:42:10', '2022-08-14 17:42:10'),
(2, 1, 3, 'Example (PDO)', NULL, '<?php\r\n$servername = \"localhost\";\r\n$username = \"username\";\r\n$password = \"password\";\r\n$dbname = \"myDBPDO\";\r\n\r\ntry {\r\n  $conn = new PDO(\"mysql:host=$servername;dbname=$dbname\", $username, $password);\r\n  // set the PDO error mode to exception\r\n  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);\r\n\r\n  $sql = \"UPDATE MyGuests SET lastname=\'Doe\' WHERE id=2\";\r\n\r\n  // Prepare statement\r\n  $stmt = $conn->prepare($sql);\r\n\r\n  // execute the query\r\n  $stmt->execute();\r\n\r\n  // echo a message to say the UPDATE succeeded\r\n  echo $stmt->rowCount() . \" records UPDATED successfully\";\r\n} catch(PDOException $e) {\r\n  echo $sql . \"<br>\" . $e->getMessage();\r\n}\r\n\r\n$conn = null;\r\n?>', '2022-08-14 17:45:10', '2022-08-14 17:45:10'),
(4, 1, 7, 'a nisi omnis nulla, iure quam libero ab', NULL, 'Lorem ipsum, dolor.\r\nLorem ipsum dolor, sit, amet consectetur adipisicing elit. Commodi ad exercitationem earum expedita eaque dolorum tempore. Voluptas vero quam, sapiente nam porro quo unde, esse.\r\n\r\nLorem ipsum dolor sit amet.\r\nLorem ipsum dolor sit amet consectetur, adipisicing elit. Commodi, iste pariatur asperiores soluta facilis ducimus vero non minus architecto temporibus, sapiente iusto vitae est officiis quas, laudantium veritatis, labore error. Ea, quisquam rerum at ipsum nemo molestias voluptas laboriosam pariatur excepturi? Debitis omnis at explicabo eligendi, dolorem tempore suscipit eos!\r\n\r\n\r\n<script src=\"https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js\" integrity=\"sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D\" crossorigin=\"anonymous\" async></script>\r\n\r\n<script src=\"https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js\" integrity=\"sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D\" crossorigin=\"anonymous\" async></script>\r\n\r\n<script src=\"https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js\" integrity=\"sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D\" crossorigin=\"anonymous\" async></script>\r\n                    \r\nLorem ipsum dolor, sit.\r\nLorem ipsum dolor sit, amet consectetur adipisicing, elit. Similique dolorem nesciunt explicabo quas temporibus accusamus enim possimus excepturi eius consequatur magnam libero voluptate quaerat placeat ipsa deserunt id, quia architecto qui sapiente. Enim, similique, cupiditate.\r\n\r\nLorem ipsum dolor sit amet consectetur adipisicing elit. Earum molestias odio natus quisquam minus magnam itaque exercitationem non cum est.\r\n\r\nTypage des modules\r\nTypage des modules\r\nLorem, ipsum dolor, sit amet consectetur adipisicing elit. Aut earum, rerum eum? Numquam quos ut tempora officiis, ipsum delectus, fuga alias, porro adipisci perspiciatis reprehenderit.\r\n\r\nLorem ipsum dolor, sit amet consectetur.\r\nLorem ipsum dolor, sit amet consectetur adipisicing elit. Ad asperiores suscipit iure, doloribus sint odio quae! Accusamus voluptate excepturi soluta beatae quas optio similique vitae, sunt.\r\n\r\nLorem ipsum, dolor sit, amet Deserunt, aperiam :\r\n\r\nLorem ipsum dolor sit amet consectetur, adipisicing, elit. Placeat, repudiandae!\r\nLorem ipsum dolor sit amet consectetur adipisicing elit. Similique eum fugit est, sunt suscipit quibusdam?\r\nLorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi!\r\nLorem, ipsum dolor, sit amet consectetur adipisicing elit. Ratione, quibusdam, tempora?\r\nLorem, ipsum dolor sit amet consectetur adipisicing elit. Cumque tempora amet exercitationem omnis iure quisquam est hic necessitatibus ea.\r\n\r\nLorem, ipsum dolor.\r\nLorem ipsum dolor sit, amet consectetur, adipisicing elit. Veritatis, eius iure pariatur similique deserunt odio non veniam tempore error sit! Accusantium eius quo tempore non, corporis, modi dolorem, beatae quidem id reiciendis numquam. Suscipit natus a nisi omnis nulla, iure quam libero ab? Obcaecati quos harum consequuntur aliquid, libero similique cupiditate accusantium laborum nemo quis debitis? Fuga voluptate ea maxime.\r\n\r\nLorem, ipsum.\r\nLorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat blanditiis, obcaecati distinctio architecto fugiat quae quod a nesciunt ad aspernatur. Exercitationem eaque, consequatur aliquid id repellendus neque unde rerum tempora?\r\n\r\nLorem, ipsum dolor.\r\nLorem ipsum, dolor sit, amet consectetur adipisicing elit. Assumenda fuga impedit laborum distinctio maxime cum consequatur, inventore saepe quam deserunt, dignissimos, eveniet magni fugiat nihil libero ab?', '2022-08-15 18:21:51', '2022-08-15 18:21:51'),
(6, 1, 5, 'df fd hrfdh h d ', NULL, 'qz efg gd dfhffd  df', '2022-08-15 20:39:32', '2022-08-15 20:39:32'),
(9, 1, 3, 'Create new article Create new articlerr4', NULL, 'Create new article Create new article', '2022-08-15 21:09:21', '2022-08-15 21:09:21');

-- --------------------------------------------------------

--
-- Structure de la table `article_tags`
--

CREATE TABLE `article_tags` (
  `article_id` int NOT NULL,
  `tags_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `article_tags`
--

INSERT INTO `article_tags` (`article_id`, `tags_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 7),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 7),
(9, 2),
(9, 3),
(9, 4),
(9, 5),
(9, 7);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(6, 'Development'),
(4, 'General'),
(7, 'Maths'),
(3, 'RISY'),
(5, 'Symfony');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int NOT NULL,
  `article_id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `article_id`, `name`, `email`, `content`, `created_at`) VALUES
(1, 1, 'Yassin', 'test@exemple.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page.\r\n\r\nIntéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-14 17:56:20'),
(2, 2, 'Yassin 2', 'test@exemple.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page.\r\n\r\nIntéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-14 17:57:20'),
(3, 1, 'Yassin 3', 'test@exemple.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page.\r\n\r\nIntéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-14 17:58:20'),
(5, 1, 'RISY', 'leyassino@gmail.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page. Intéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-15 16:11:06'),
(6, 1, 'Franck Lowe', 'test2@test.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page. Intéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-15 16:11:40'),
(7, 2, 'Franck Lowe', 'user@example.com', 'Nous savons maintenant comment faire pour envoyer des paramètres vers une autre page. Intéressons-nous maintenant à la page qui réceptionne les paramètres.', '2022-08-15 16:12:05');

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `created_at`) VALUES
(1, 'Yassin', 'elyassino@gmail.com', 'sqdg hjgqsdh jkgqdj qshg', 'qsdg qjksgj qkjgji igjifqs gi fkjgjf jkhji fyqih qkjfyshkh ysdfhsdj jhsdjghjky sd\r\nsdfgjkbg bjkhdgs', '2022-08-14 07:46:45'),
(2, 'Yassin', 'elyassino@gmail.com', 'sqdg hjgqsdh jkgqdj qshg', 'qsdg qjksgj qkjgji igjifqs gi fkjgjf jkhji fyqih qkjfyshkh ysdfhsdj jhsdjghjky sd\r\nsdfgjkbg bjkhdgs', '2022-08-14 15:46:45'),
(3, 'Yassin', 'elyassino@gmail.com', 'sqdg hjgqsdh jkgqdj qshg', 'qsdg qjksgj qkjgji igjifqs gi fkjgjf jkhji fyqih qkjfyshkh ysdfhsdj jhsdjghjky sd\r\nsdfgjkbg bjkhdgs', '2022-08-14 16:46:45'),
(4, 'Yassin', 'elyassino@gmail.com', 'sqdg hjgqsdh jkgqdj qshg', 'qsdg qjksgj qkjgji igjifqs gi fkjgjf jkhji fyqih qkjfyshkh ysdfhsdj jhsdjghjky sd\r\nsdfgjkbg bjkhdgs', '2022-08-14 17:46:45'),
(5, 'RISY', 'leyassino@gmail.com', 'ezvghevs ezfyez', 'Are you looking for visibility on YouTube (and Twitter) with an audience of French-speaking developers? Do not hesitate to contact me ! Propose a partnership', '2022-08-15 12:57:09'),
(6, 'Code Ouvert', 'user@example.com', 'ezvghevs ezfyez', 'Are you looking for visibility on YouTube (and Twitter) with an audience of French-speaking developers? Do not hesitate to contact me ! Propose a partnership', '2022-08-15 12:59:28'),
(7, 'RISY', 'leyassino@gmail.com', 'ezvghevs ezfyez', 'Are you looking for visibility on YouTube (and Twitter) with an audience of French-speaking developers? Do not hesitate to contact me ! Propose a partnership', '2022-08-15 13:14:10'),
(8, 'RISY', 'leyassino@gmail.com', 'ezvghevs ezfyez', 'Are you looking for visibility on YouTube (and Twitter) with an audience of French-speaking developers? Do not hesitate to contact me ! Propose a partnership\r\n\r\n', '2022-08-15 14:57:19');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(5, 'Java'),
(4, 'JavaScript'),
(3, 'PHP'),
(2, 'Pia Soft'),
(7, 'RISY21156');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `roles`, `password`, `created_at`) VALUES
(1, 'admin1', '[\"ROLE_ADMIN\"]', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '2022-08-13 20:41:31'),
(2, 'admin2', '[\"ROLE_ADMIN\"]', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '2022-08-13 20:42:31'),
(3, 'admin3', '[\"ROLE_ADMIN\"]', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '2022-08-13 20:43:31');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_BFDD31682B36786B` (`title`),
  ADD KEY `IDX_BFDD3168A76ED395` (`user_id`),
  ADD KEY `IDX_BFDD316812469DE2` (`category_id`);

--
-- Index pour la table `article_tags`
--
ALTER TABLE `article_tags`
  ADD PRIMARY KEY (`article_id`,`tags_id`),
  ADD KEY `IDX_DFFE13277294869C` (`article_id`),
  ADD KEY `IDX_DFFE13278D7B4FB4` (`tags_id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_3AF346685E237E06` (`name`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5F9E962A7294869C` (`article_id`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_6FBC94265E237E06` (`name`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1483A5E9F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `FK_BFDD316812469DE2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BFDD3168A76ED395` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `article_tags`
--
ALTER TABLE `article_tags`
  ADD CONSTRAINT `FK_DFFE13277294869C` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DFFE13278D7B4FB4` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_5F9E962A7294869C` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
